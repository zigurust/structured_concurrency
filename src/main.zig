const PollTag = enum {
    Pending,
    Ready,
};

fn Poll(comptime T: type) type {
    return union(PollTag) {
        Pending: void,
        Ready: T,
    };
}

test "test Poll::Ready" {
    // Arrange
    const PollU32 = Poll(u32);
    const poll = PollU32{ .Ready = 5 };
    const expectEqual = @import("std").testing.expectEqual;
    // Act
    // Assert
    switch (poll) {
        PollTag.Ready => |value| try expectEqual(5, value),
        PollTag.Pending => unreachable,
    }
}

pub const WakerVTable = struct {
    wake: *const fn (self: *anyopaque) void,
};

pub const Waker = struct {
    ptr: *anyopaque,
    vtable: WakerVTable,
    const Self = @This();
    pub fn new(vtable: WakerVTable) Self {
        return .{vtable};
    }
    pub fn wake(self: *Self) void {
        self.vtable.wake(self.ptr);
    }
};

test "Waker interface" {
    // Arrange
    const SimpleWaker = struct {
        a: u8 = 0,
        const Self = @This();
        pub fn wake(self: *anyopaque) void {
            const self_ptr: *Self = @ptrCast(@alignCast(self));
            self_ptr.a += 1;
        }
    };
    var simpleWaker = SimpleWaker{};
    const simpleWakerPtr = &simpleWaker;
    const wakerVTable = WakerVTable{ .wake = SimpleWaker.wake };
    var waker = Waker{ .ptr = simpleWakerPtr, .vtable = wakerVTable };
    // Act
    waker.wake();
    waker.wake();
    // Assert
    const expectEqual = @import("std").testing.expectEqual;
    try expectEqual(2, simpleWaker.a);
}

pub const Context = struct {
    waker: Waker,
    pub fn fromWaker(waker: Waker) Context {
        return Context{ .waker = waker };
    }
};

test "Context" {
    // Arrange
    const SimpleWaker = struct {
        a: u8 = 0,
        const Self = @This();
        pub fn wake(self: *anyopaque) void {
            const self_ptr: *Self = @ptrCast(@alignCast(self));
            self_ptr.a += 1;
        }
    };
    var simpleWaker = SimpleWaker{};
    const simpleWakerPtr = &simpleWaker;
    const wakerVTable = WakerVTable{ .wake = SimpleWaker.wake };
    const waker = Waker{ .ptr = simpleWakerPtr, .vtable = wakerVTable };
    // Act
    var context = Context.fromWaker(waker);
    context.waker.wake();
    context.waker.wake();
    context.waker.wake();
    // Assert
    const expectEqual = @import("std").testing.expectEqual;
    try expectEqual(3, simpleWaker.a);
}

fn FutureVTable(comptime T: type) type {
    return struct {
        poll: *const fn (self: *anyopaque, context: *Context) Poll(T),
        poll_cancel: *const fn (self: *anyopaque, context: *Context) Poll(?T),
    };
}

pub fn Future(comptime T: type) type {
    return struct {
        ptr: *anyopaque,
        vtable: FutureVTable(T),
        const Self = @This();
        pub fn poll(self: *Self, context: *Context) Poll(T) {
            return self.vtable.poll(self.ptr, context);
        }
        pub fn poll_cancel(self: *Self, context: *Context) !Poll(?T) {
            return self.vtable.poll_cancel(self.ptr, context);
        }
    };
}

test "Future" {
    // Arrange
    const SimpleWaker = struct {
        a: u8 = 0,
        const Self = @This();
        pub fn wake(self: *anyopaque) void {
            const self_ptr: *Self = @ptrCast(@alignCast(self));
            self_ptr.a += 1;
        }
    };
    var simpleWaker = SimpleWaker{};
    const simpleWakerPtr = &simpleWaker;
    const wakerVTable = WakerVTable{ .wake = SimpleWaker.wake };
    const waker = Waker{ .ptr = simpleWakerPtr, .vtable = wakerVTable };
    var context = Context.fromWaker(waker);
    const FutureStateTags = enum {
        Initialized,
        Started,
        State1,
        State2,
        Ready,
    };
    const FutureState = union(FutureStateTags) {
        Initialized: void,
        Started: u8,
        State1: u32,
        State2: u64,
        Ready: bool,
    };
    const FuturePoll = Poll(bool);
    const SimpleFuture = struct {
        state: FutureState = FutureState.Initialized,
        const Self = @This();
        pub fn poll(self_ptr: *anyopaque, ctx: *Context) Poll(bool) {
            const self: *Self = @ptrCast(@alignCast(self_ptr));
            switch (self.state) {
                FutureState.Initialized => {
                    self.state = .{ .Started = 0 };
                    ctx.waker.wake();
                    return FuturePoll.Pending;
                },
                FutureState.Started => |value| {
                    self.state = .{ .State1 = @intCast(value + 1) };
                    ctx.waker.wake();
                    return FuturePoll.Pending;
                },
                FutureState.State1 => |value| {
                    self.state = .{ .State2 = @intCast(value * 2) };
                    ctx.waker.wake();
                    return FuturePoll.Pending;
                },
                FutureState.State2 => |value| {
                    const boolValue = value > 0;
                    self.state = .{ .Ready = boolValue };
                    return FuturePoll{ .Ready = boolValue };
                },
                FutureState.Ready => |_| {
                    unreachable;
                },
            }
        }
        pub fn poll_cancel(self_ptr: *anyopaque, ctx: *Context) Poll(?bool) {
            _ = self_ptr;
            _ = ctx;
            const CancelPoll = Poll(?bool);
            return CancelPoll{ .Ready = true };
        }
    };
    var simpleFuture = SimpleFuture{};
    const futurePtr = &simpleFuture;
    const SimpleFutureVTable = FutureVTable(bool);
    const fvtable = SimpleFutureVTable{
        .poll = SimpleFuture.poll,
        .poll_cancel = SimpleFuture.poll_cancel,
    };
    var future = Future(bool){
        .ptr = futurePtr,
        .vtable = fvtable,
    };
    const expectEqual = @import("std").testing.expectEqual;
    // Act
    var futureState = future.poll(&context);
    try expectEqual(FuturePoll.Pending, futureState);
    try expectEqual(FutureState{ .Started = 0 }, simpleFuture.state);
    futureState = future.poll(&context);
    try expectEqual(FuturePoll.Pending, futureState);
    try expectEqual(FutureState{ .State1 = 1 }, simpleFuture.state);
}
