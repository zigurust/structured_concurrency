# async Zig or futures in Zig

An experiment to use futures in Zig, inspired by futures in Rust.
Futures can be used for asynchronous programming.
The main goals are:

* Improve my Zig skills
* Deepen my knowledge of the async programming model in Rust (and its implementation details)
* Having fun with Zig

## Supported Zig version

0.12.0

## Alternatives

Contains a list of async programming alternatives, written in Zig:

* [libxev](https://github.com/mitchellh/libxev)
* [zap](https://github.com/kprotty/zap)
* [pike](https://github.com/lithdew/pike)

## Resources

* [A friendly abstraction over io_uring and kqueue](https://tigerbeetle.com/blog/a-friendly-abstraction-over-iouring-and-kqueue)

## Interesting next steps

One of the main reasons to use the `async/await` abstraction in programming languages that support this pattern is, that the developer is lifted from the relatively simply but error-prone (like forgetting something) implementation of state machines for the coroutines or futures. 
The ideas I follow here, is not very ergonomic and it requires to implement the aforementioned state machines by hand.
Therefore I propose an idea here, where one would create the content of a future by combining the functions that realize the individual parts of the state machine, i.e., a function for each state. Something along the lines of:

```zig
pub fn create_future(comptime T1: type, part1: fn(Context)-> T1, comptime T2: part2: fn(Context, T) -> T2) Future(T2) { /*...*/ }
```

To achieve this, the following things need to work, AFAIK:

* The possibility to create a tagged union during `comptime`, that defines the state of the created future.
* The possibility to create the future passed on the tagged union and that has all the defined functions callable. So that the `poll` method can call the correct function based on the current value in the tagged union.
